//
//  OrderCell.swift
//  ServiceTechnologieseJH
//
//  Created by jose fernando herrera montoya on 6/07/20.
//  Copyright © 2020 Jose Herrera. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MaterialComponents.MaterialActivityIndicator

class OrderCell: UITableViewCell {
 
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var separator: UIView!
    
    @IBOutlet weak var service: UILabel!
    
    @IBOutlet weak var phoneTitle: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var serviceTitle: UILabel!
    
    override func layoutSubviews() {
 
        view.backgroundColor = .white

        view.layer.cornerRadius = 6.0

        view.layer.shadowColor = UIColor.gray.cgColor

        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)

        view.layer.shadowRadius = 6.0

        view.layer.shadowOpacity = 0.7
        
        view.snp.makeConstraints{ (make) -> Void in
                make.width.equalTo(contentView).offset(-32)
                make.height.equalTo(contentView).offset(-22)
                make.center.equalTo(view.center)
            }
        
        name.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(view.snp.top).offset(10)
            make.left.equalTo(view.snp.left).offset(16)
            make.right.equalTo(view.snp.right).offset(-16)
            make.height.equalTo(20)
        }
    
        address.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(name.snp.bottom)
            make.left.equalTo(view.snp.left).offset(16)
            make.right.equalTo(view.snp.right).offset(-16)
             make.height.equalTo(20)
        }
        
        separator.snp.makeConstraints{ (make) -> Void in
              
            make.top.equalTo(address.snp.bottom).offset(5)
            make.centerX.equalTo(view)
            make.width.equalTo(1)
            make.bottom.equalTo(view.snp.bottom).offset(-16)
        }
        
        serviceTitle.snp.makeConstraints{ (make) -> Void in
        
            make.top.equalTo(address.snp.bottom).offset(5)
            make.left.equalTo(view).offset(16)
            make.right.equalTo(separator).offset(-10)
            
        }
        
        service.snp.makeConstraints{ (make) -> Void in
             
                 make.top.equalTo(serviceTitle.snp.bottom).offset(5)
            make.left.equalTo(view.snp.left).offset(16)
            make.right.equalTo(separator.snp.left).offset(-10)
                 
             }
        
        phoneTitle.snp.makeConstraints{ (make) -> Void in
             
                 make.top.equalTo(address.snp.bottom).offset(5)
            make.left.equalTo(separator.snp.right).offset(10)
            make.right.equalTo(view.snp.right).offset(-16)
                 
             }
        phone.snp.makeConstraints{ (make) -> Void in
                    
            make.top.equalTo(phoneTitle.snp.bottom).offset(5)
            make.left.equalTo(separator.snp.right).offset(10)
            make.right.equalTo(view.snp.right).offset(-16)
                        
        }
    }
    
}
