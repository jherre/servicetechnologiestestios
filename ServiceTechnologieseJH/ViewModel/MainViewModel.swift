//
//  MainViewModel.swift
//  ServiceTechnologieseJH
//
//  Created by jose fernando herrera montoya on 4/07/20.
//  Copyright © 2020 Jose Herrera. All rights reserved.
//

import Foundation
import Alamofire


public class Order {

    var first_name : String!
    var last_name : String!
    var service_name : String!
    var mobile : String!
    var address1 : String!
    var address2 : String!
    var o_id :String!

    init(orderDictionary : NSDictionary) {
    
        first_name = orderDictionary.object(forKey: "first_name") as? String
        last_name = orderDictionary.object(forKey: "last_name") as? String
        service_name = orderDictionary.object(forKey: "service_name") as? String
        mobile = orderDictionary.object(forKey: "mobile") as? String
        address1 = orderDictionary.object(forKey: "address1") as? String
        address2 = orderDictionary.object(forKey: "address2") as? String
        o_id = orderDictionary.object(forKey: "o_id") as? String
    
    }
    
}

public class OrderDetails {

    var first_name : String!
    var last_name : String!
    var service_name : String!
    var mobile : String!
    var address1 : String!
    var address2 : String!
    var o_id :String!
    
    var open_date : String!
    var allotted_hours : String!
    var apntmnt_date : String!
    var apntmnt_time : String!
    var home_phone : String!
    var country : String!
    var state : String!
    var city : String!
    var zipcode : String!
    var claim_type : String!
    var email : String!
    var lat : String!
    var lng : String!
    var client_order_no : String!
    
    init(orderDictionary : NSDictionary) {
    
        first_name = orderDictionary.object(forKey: "first_name") as? String
        last_name = orderDictionary.object(forKey: "last_name") as? String
        service_name = orderDictionary.object(forKey: "service_name") as? String
        mobile = orderDictionary.object(forKey: "mobile") as? String
        address1 = orderDictionary.object(forKey: "address1") as? String
        address2 = orderDictionary.object(forKey: "address2") as? String
        o_id = orderDictionary.object(forKey: "o_id") as? String
        open_date = orderDictionary.object(forKey: "open_date") as? String
        allotted_hours = orderDictionary.object(forKey: "allotted_hours") as? String
        apntmnt_date = orderDictionary.object(forKey: "apntmnt_date") as? String
        apntmnt_time = orderDictionary.object(forKey: "apntmnt_time") as? String
        home_phone = orderDictionary.object(forKey: "home_phone") as? String
        country = orderDictionary.object(forKey: "country") as? String
        state = orderDictionary.object(forKey: "state") as? String
        city = orderDictionary.object(forKey: "city") as? String
        zipcode = orderDictionary.object(forKey: "zipcode") as? String
        claim_type = orderDictionary.object(forKey: "claim_type") as? String
        email = orderDictionary.object(forKey: "email") as? String
        lat = orderDictionary.object(forKey: "lat") as? String
        lng = orderDictionary.object(forKey: "lng") as? String
        client_order_no = orderDictionary.object(forKey: "client_order_no") as? String
        
    }
    
}

class MainViewModel {
    
    var access_token : String!
    var userId : String!
    var orderList : [Order]!
    var orderDetails : OrderDetails!
        
    func login(main : MainProtocol, loginId : String, password : String ) {
            
        let headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        
        let params : Parameters = ["login" : "{\"app_version\" : \"1.0.0\",\"os_version\" : \"12.0.1\", \"os_id\" : \"1\", " +
          "\"device_id\" : \"ADE58686-AAA0-4D30-A0CC-1DFAC9ACFCBA\",\"device_model\" : \"iPhone11,8\", " +
          "\"password\" : \"" + password + "\", \"device_name\" : \"iPhone11,8\", \"login_user_id\" : \"" +
          loginId + "\"}"]

            AF.request("https://dev3.servicehubcrm.net/mobile_api/v14/mapi/login", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers)
                
                .responseJSON { response in
                                    
                    let JSON = response.value as! NSDictionary
                    
                    let json = JSON.object(forKey: "response") as! NSDictionary
                    let data = json.object(forKey: "data") as! NSDictionary
                    self.access_token = data.object(forKey: "access_token") as? String
                    self.userId = data.object(forKey: "user_id") as? String
                    if(self.access_token != nil) {
                        
                        main.onLogin(error : "")
                        
                    }else {
                        
                        let error = json.object(forKey: "message") as? String
                        
                        main.onLogin(error: error!)
                        
                    }
                                        
            }
            
        }
    
    func getOrders(orders : OrdersProtocol) {
            
        let headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded", "access_token" : access_token , "user_id" : userId, "device_id" : "94109DD7-12E5-4226-A134-156900C1DC0E"]
        
        let params : Parameters = ["getOrders" : "{\"user_id\" : \"" + userId + "\",\"limit\" : \"200\", \"page\" : \"1\", " +
        "\"list_type\" : \"all\"}"]

            AF.request("https://dev3.servicehubcrm.net/mobile_api/v14/mapi/getOrders", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers)
                
                .responseJSON { response in
                                    
                    let JSON = response.value as! NSDictionary
                    let json = JSON.object(forKey: "response") as! NSDictionary
                    let data = json.object(forKey: "data") as! NSDictionary
                    let array = data.object(forKey: "orderList") as? NSArray
                    self.orderList = [Order]()
                    
                    for items in array! {
                    
                        self.orderList.append(Order.init(orderDictionary: items as! NSDictionary))
                        
                    }
                    
                    if(!self.orderList.isEmpty) {
                        
                        orders.onGetOrders(orderList : self.orderList, error : "")
                        
                    }else {
                        
                        let error = JSON.object(forKey: "message") as? String
                        
                        orders.onGetOrders(orderList : self.orderList, error: error!)
                        
                    }
                                        
            }
            
        }
    
    func getOrderDetails(getOrderDetails : OrderDetailsProtocol, orderIdRow : Int) {
            
     let headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded", "access_token" : access_token , "user_id" : userId, "device_id" : "94109DD7-12E5-4226-A134-156900C1DC0E"]
        
        let order_id = orderList[orderIdRow].o_id!
        let body2 = "\",\"order_id\" : \"" + order_id + "\"}"
        
        let body1 = "{\"user_id\" : \"" + userId + body2
        
        let params : Parameters = ["getOrderDetails" : body1]

            AF.request("https://dev3.servicehubcrm.net/mobile_api/v14/mapi/getOrderDetails", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers)
                
                .responseJSON { response in
                                    
                    let JSON = response.value as! NSDictionary
                   
                    let json = JSON.object(forKey: "response") as! NSDictionary
                    let data = json.object(forKey: "data") as! NSDictionary
                    let dataOrder = data.object(forKey: "orderDetail") as! NSDictionary
                    self.orderDetails = OrderDetails(orderDictionary: dataOrder)
                        
                    if(self.orderDetails != nil) {
                        
                        getOrderDetails.onGetOrderDetails(error : "")
                        
                    }else {
                        
                        let error = json.object(forKey: "message") as? String
                        
                        getOrderDetails.onGetOrderDetails(error: error!)
                        
                    }
                                        
            }
            
        }
}


protocol MainProtocol {
        
    func onLogin(error : String)
        
}

protocol OrdersProtocol {
        
    func onGetOrders(orderList : [Order], error : String)
        
}

protocol OrderDetailsProtocol {
        
    func onGetOrderDetails(error : String)
        
}
