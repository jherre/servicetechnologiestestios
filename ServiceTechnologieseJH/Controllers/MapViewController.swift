//
//  MapViewController.swift
//  SunbeltTest
//
//  Created by jose fernando herrera montoya on 28/06/20.
//  Copyright © 2020 SunbeltSAS. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

// MARK: - showRouteOnMap
    @IBOutlet weak var distance: UILabel!
    
    // MARK: - showRouteOnMap
    @IBOutlet weak var duration: UILabel!
    
    var startLat : Double!
    var startLng : Double!
    var destinationLat : Double!
    var destinationLng : Double!
    var locationManager:CLLocationManager!
    
func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {

    let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
    let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

    let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
    let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

    let sourceAnnotation = MKPointAnnotation()

    if let location = sourcePlacemark.location {
        sourceAnnotation.coordinate = location.coordinate
    }

    let destinationAnnotation = MKPointAnnotation()

    if let location = destinationPlacemark.location {
        destinationAnnotation.coordinate = location.coordinate
    }

    self.map.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )

    let directionRequest = MKDirections.Request()
    directionRequest.source = sourceMapItem
    directionRequest.destination = destinationMapItem
    directionRequest.transportType = .automobile

    // Calculate the direction
    let directions = MKDirections(request: directionRequest)

    directions.calculate {
        (response, error) -> Void in

        guard let response = response else {
            if let error = error {
                print("Error: \(error)")
            }

            return
        }

        let route = response.routes[0]
        
        var durationnCal = route.expectedTravelTime
        
        var string = ""
        
        if(durationnCal <= 3600) {
            
            durationnCal = durationnCal / 60
            
           string = "Expected Time Of Arrival: " + String( Int(durationnCal.rounded())) + " mins"
            
        }else {
            
             let minutes = durationnCal / 60.truncatingRemainder(dividingBy: 60) * 60
            
            durationnCal = durationnCal / 60 / 60
           
            string = "Expected Time Of Arrival: " + String(Int(durationnCal.rounded())) + " hours " + String(Int(minutes.rounded())) + " mins"
        }

        var string2 = ""
        
        var distanceCal = route.distance
        
        if(distanceCal <= 1000) {
            
           string2 = "Total Distance: " + String(Int(distanceCal.rounded())) + " m."
            
        }else {
            
            let meters = distanceCal.truncatingRemainder(dividingBy: 1000) * 1000
              
            distanceCal = distanceCal / 1000
           
            string2 = "Total Distance: " + String(Int(durationnCal.rounded())) + " km " + String(Int(meters.rounded())) + " m"
        }

        self.distance.text = string2
     
        self.duration.text = string
        
        self.map.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

        let rect = route.polyline.boundingMapRect
        
        self.map.setRegion(MKCoordinateRegion(rect), animated: true)
    }
}

func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

    let renderer = MKPolylineRenderer(overlay: overlay)

    renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)

    renderer.lineWidth = 5.0

    return renderer
}
    
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       
        let pickup = locations[0].coordinate
        
        let destination = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng)
               
        showRouteOnMap(pickupCoordinate: pickup, destinationCoordinate: destination)
     
    }

    @IBOutlet weak var map: MKMapView!
    
        override func viewDidLoad() {
            
            map.delegate = self

            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
         
    }
    
}
