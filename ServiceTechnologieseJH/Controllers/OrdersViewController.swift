//
//  OrdersViewController.swift
//  ServiceTechnologieseJH
//
//  Created by jose fernando herrera montoya on 5/07/20.
//  Copyright © 2020 Jose Herrera. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialActivityIndicator

class OrdersViewController: UITableViewController, OrdersProtocol {
    
    var mainViewModel : MainViewModel?
    var orderList : [Order]?
    
    let activityIndicator = MDCActivityIndicator()
    
    var row : Int?
    @IBOutlet var table: UITableView!
    
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
             
        mainViewModel!.getOrders(orders: self)
      
    }
    
    func onGetOrders(orderList : [Order], error: String) {
        
        self.orderList = orderList
         
        table.reloadData()
        
        activityIndicator.stopAnimating()

       
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        row = indexPath.row
     
        performSegue(withIdentifier: "orderDetailsSegue", sender: self)
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList?.count ?? 0
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrderCell
        
        cell.name.text = orderList![indexPath.row].first_name
          
        cell.address.text = orderList![indexPath.row].address1 ?? "" + " " + orderList![indexPath.row].address2
        
        cell.service.text = orderList![indexPath.row].service_name
        
        cell.phone.text = orderList![indexPath.row].mobile
        
        return cell
           
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          
        let viewController = segue.destination as? OrderDetailsViewController
               
        viewController!.mainViewModel = self.mainViewModel
               
        viewController!.row = self.row
        
       }

    
}
