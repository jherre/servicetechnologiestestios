//
//  ViewController.swift
//  ServiceTechnologieseJH
//
//  Created by jose fernando herrera montoya on 4/07/20.
//  Copyright © 2020 Jose Herrera. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class LoginViewController: UIViewController, UITextFieldDelegate, MainProtocol{

    @IBOutlet weak var loginId: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    var mainViewModel : MainViewModel!
    
    let activityIndicator = MDCActivityIndicator()
    
    @IBAction func login(_ sender: Any) {
        
        activityIndicator.startAnimating()
        
        if(loginId.text!.isEmpty) {
              let error = "Empty Login Id"
              let alert = UIAlertController(title: "Alert", message: error, preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
             self.present(alert, animated: true, completion: nil)
        }else if(password.text!.count < 5){
            let error = "Password must have more than 5 characters"
            let alert = UIAlertController(title: "Alert", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else {
        
            mainViewModel.login(main: self, loginId: loginId.text!, password: password.text!)
        
        }

    }
    
    func onLogin(error: String) {
       
        if(error != "") {
        
        let alert = UIAlertController(title: "Alert", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    
            self.present(alert, animated: true, completion: nil)
        
        }else {

            performSegue(withIdentifier: "loginSegue", sender: self)
        }
       
      activityIndicator.stopAnimating()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        let viewController = segue.destination as? UINavigationController
            
        let ordersViewController = viewController?.viewControllers.first as! OrdersViewController
        
        ordersViewController.mainViewModel = self.mainViewModel
            
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       textField.resignFirstResponder()
       if textField == loginId { // Switch focus to other text field
           password.becomeFirstResponder()
       }else{
        
            dismissKeyboard()
        }
       return true
   }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewModel = MainViewModel()
        activityIndicator.sizeToFit()
        view.addSubview(activityIndicator)



        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        loginId.delegate = self
        password.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);

           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
    }

    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -350 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
