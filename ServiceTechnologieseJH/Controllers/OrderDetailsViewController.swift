//
//  OrderDetailsController.swift
//  ServiceTechnologieseJH
//
//  Created by jose fernando herrera montoya on 7/07/20.
//  Copyright © 2020 Jose Herrera. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialActivityIndicator

class OrderDetailsViewController: UIViewController, OrderDetailsProtocol {
    
    var mainViewModel : MainViewModel?
    
    var row : Int?
    
    let activityIndicator = MDCActivityIndicator()
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var topContainerView: UIView!
    
    @IBOutlet weak var topContainerBackView: UIView!
    
    @IBOutlet weak var bottomContainerView: UIView!
    
    @IBOutlet weak var bottomContainerBackView: UIView!
    
    @IBOutlet weak var navigateToCustomer: UIButton!
    
    @IBOutlet weak var firstNameTitle: UILabel!
    
    @IBOutlet weak var firstName: UILabel!
    
    @IBOutlet weak var bottomSeparator: UIView!
    
    @IBOutlet weak var bottomHorizontalSeparator1: UIView!
    
    @IBOutlet weak var lastNameTitle: UILabel!
      
    @IBOutlet weak var lastName: UILabel!
    
    @IBOutlet weak var bottomHorizontalSeparator2: UIView!
    
    @IBOutlet weak var address1Title: UILabel!
      
    @IBOutlet weak var address1: UILabel!
    
    @IBOutlet weak var bottomHorizontalSeparator3: UIView!
    
    @IBOutlet weak var address2Title: UILabel!
        
    @IBOutlet weak var address2: UILabel!
      
    @IBOutlet weak var bottomHorizontalSeparator4: UIView!
    
    @IBOutlet weak var countryTitle: UILabel!
        
    @IBOutlet weak var country: UILabel!
      
    @IBOutlet weak var bottomHorizontalSeparator5: UIView!
    
    @IBOutlet weak var stateTitle: UILabel!
         
    @IBOutlet weak var state: UILabel!
       
    @IBOutlet weak var bottomHorizontalSeparator6: UIView!
    
    @IBOutlet weak var cityTitle: UILabel!
            
    @IBOutlet weak var city: UILabel!
          
    @IBOutlet weak var bottomHorizontalSeparator7: UIView!
    
    @IBOutlet weak var zipTitle: UILabel!
            
    @IBOutlet weak var zip: UILabel!
          
    @IBOutlet weak var bottomHorizontalSeparator8: UIView!
    
    @IBOutlet weak var phoneTitle: UILabel!
            
    @IBOutlet weak var phone: UILabel!
          
    @IBOutlet weak var bottomHorizontalSeparator9: UIView!
    
    @IBOutlet weak var homeTitle: UILabel!
            
    @IBOutlet weak var home: UILabel!
          
    @IBOutlet weak var bottomHorizontalSeparator10: UIView!
    
    @IBOutlet weak var claimTypeTitle: UILabel!
            
    @IBOutlet weak var claimType: UILabel!
          
    @IBOutlet weak var bottomHorizontalSeparator11: UIView!
    
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var serviceTypeTitle: UILabel!
    
    @IBOutlet weak var serviceType: UILabel!
    
    @IBOutlet weak var mobileTitle: UILabel!
    
    @IBOutlet weak var mobile: UILabel!
    
    @IBOutlet weak var topSeparator1: UIView!
    
    @IBOutlet weak var openDateTitle: UILabel!
       
    @IBOutlet weak var openDate: UILabel!
    
    @IBOutlet weak var allotedHoursTitle: UILabel!
       
    @IBOutlet weak var allotedHours: UILabel!
    
    @IBOutlet weak var topSeparator2: UIView!
    
    @IBOutlet weak var appointmentTitle: UILabel!
       
    @IBOutlet weak var appointment: UILabel!
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
             
        mainViewModel!.getOrderDetails(getOrderDetails: self, orderIdRow : row!)
      
        topContainerView.layer.cornerRadius = 9.0
      
        bottomContainerView.layer.cornerRadius = 9.0
        
        navigateToCustomer.addTarget(self, action: #selector(goMap), for: .touchUpInside)
      
        let color = UIColor(red:99/255, green:99/255, blue:99/255, alpha: 1).cgColor
        
        topContainerBackView.layer.borderWidth = 1
        topContainerBackView.layer.borderColor = color
        bottomContainerBackView.layer.borderWidth = 1
        bottomContainerBackView.layer.borderColor = color
        
        topContainerView.snp.makeConstraints{ (make) -> Void in
        
            make.width.equalTo(contentView).offset(-32)
            make.centerX.equalTo(contentView)
            make.top.equalTo(contentView.snp.top).offset(60)
            make.bottom.equalTo(navigateToCustomer.snp.top).offset(-10)
        }
        topContainerBackView.snp.makeConstraints{ (make) -> Void in
        
            make.width.equalTo(topContainerView)
            make.center.equalTo(topContainerView)
            make.height.equalTo(topContainerView).offset(-12)
        }
        
        navigateToCustomer.snp.makeConstraints{ (make) -> Void in
        
            make.width.equalTo(contentView).offset(-32)
            make.center.equalTo(contentView)
            make.height.equalTo(30)
        }
        
        bottomContainerView.snp.makeConstraints{ (make) -> Void in
        
            make.width.equalTo(contentView).offset(-32)
            make.centerX.equalTo(contentView)
            make.top.equalTo(navigateToCustomer.snp.bottom).offset(10)
            make.bottom.equalTo(contentView.snp.bottom).offset(-16)
        }
        
        bottomContainerBackView.snp.makeConstraints{ (make) -> Void in
              
            make.width.equalTo(bottomContainerView)
            make.center.equalTo(bottomContainerView)
            make.height.equalTo(bottomContainerView).offset(-12)
        }
        
        firstNameTitle.snp.makeConstraints{ (make) -> Void in
              
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomContainerView).offset(16)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
            
        }

        bottomSeparator.snp.makeConstraints{ (make) -> Void in
                     
            make.top.equalTo(bottomContainerView)
            make.centerX.equalTo(bottomContainerView.snp.centerX).offset(-70)
               
        }
        firstName.snp.makeConstraints{ (make) -> Void in
                     
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomContainerView).offset(16)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            
        }
        bottomHorizontalSeparator1.snp.makeConstraints{ (make) -> Void in
                          
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(firstNameTitle.snp.bottom)
                 
        }
        lastNameTitle.snp.makeConstraints{ (make) -> Void in
                    
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator1.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                  
        }
        lastName.snp.makeConstraints{ (make) -> Void in
                           
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator1.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                  
        }
        bottomHorizontalSeparator2.snp.makeConstraints{ (make) -> Void in
                                
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(lastNameTitle.snp.bottom)
                       
        }
        
        address1Title.snp.makeConstraints{ (make) -> Void in
                          
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator2.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                        
        }
        address1.snp.makeConstraints{ (make) -> Void in
                                 
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator2.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                        
        }
        bottomHorizontalSeparator3.snp.makeConstraints{ (make) -> Void in
                                      
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(address1Title.snp.bottom)
                             
        }
        address2Title.snp.makeConstraints{ (make) -> Void in
                    
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator3.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                  
        }
        address2.snp.makeConstraints{ (make) -> Void in
                           
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator3.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                  
        }
        bottomHorizontalSeparator4.snp.makeConstraints{ (make) -> Void in
                                
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(address2Title.snp.bottom)
                       
        }
        countryTitle.snp.makeConstraints{ (make) -> Void in
                    
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator4.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                  
        }
        country.snp.makeConstraints{ (make) -> Void in
                           
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator4.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                  
        }
        bottomHorizontalSeparator5.snp.makeConstraints{ (make) -> Void in
                                
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(countryTitle.snp.bottom)
                       
        }
        stateTitle.snp.makeConstraints{ (make) -> Void in
                          
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator5.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                        
        }
        state.snp.makeConstraints{ (make) -> Void in
                                 
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator5.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                        
        }
        bottomHorizontalSeparator6.snp.makeConstraints{ (make) -> Void in
                                      
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(stateTitle.snp.bottom)
                             
        }
        cityTitle.snp.makeConstraints{ (make) -> Void in
                          
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator6.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                        
        }
        city.snp.makeConstraints{ (make) -> Void in
                                 
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator6.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                        
        }
        bottomHorizontalSeparator7.snp.makeConstraints{ (make) -> Void in
                                      
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(cityTitle.snp.bottom)
                             
        }
        zipTitle.snp.makeConstraints{ (make) -> Void in
                              
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator7.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                            
        }
        zip.snp.makeConstraints{ (make) -> Void in
                                     
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator7.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                            
        }
        bottomHorizontalSeparator8.snp.makeConstraints{ (make) -> Void in
                                          
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(zipTitle.snp.bottom)
                                 
        }
        phoneTitle.snp.makeConstraints{ (make) -> Void in
                                     
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator8.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                                   
        }
        phone.snp.makeConstraints{ (make) -> Void in
                                            
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator8.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                                   
        }
        bottomHorizontalSeparator9.snp.makeConstraints{ (make) -> Void in
                                                 
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(phoneTitle.snp.bottom)
                                        
        }
        homeTitle.snp.makeConstraints{ (make) -> Void in
                                     
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator9.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                                   
        }
        home.snp.makeConstraints{ (make) -> Void in
                                            
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator9.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                                   
        }
        bottomHorizontalSeparator10.snp.makeConstraints{ (make) -> Void in
                                                 
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(homeTitle.snp.bottom)
                                        
        }
        claimTypeTitle.snp.makeConstraints{ (make) -> Void in
                                            
            make.left.equalTo(bottomContainerView).offset(10)
            make.top.equalTo(bottomHorizontalSeparator10.snp.bottom).offset(10)
            make.right.equalTo(bottomSeparator.snp.left).offset(-10)
                            
        }
        claimType.snp.makeConstraints{ (make) -> Void in
                                                   
            make.left.equalTo(bottomSeparator.snp.right).offset(10)
            make.top.equalTo(bottomHorizontalSeparator10.snp.bottom).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
                                    
        }
        bottomHorizontalSeparator11.snp.makeConstraints{ (make) -> Void in
                                                        
            make.left.equalTo(bottomContainerView).offset(10)
            make.right.equalTo(bottomContainerView.snp.right).offset(-10)
            make.top.equalTo(claimTypeTitle.snp.bottom)
                                               
        }
        orderId.snp.makeConstraints{ (make) -> Void in
                                                         
            make.left.equalTo(topContainerBackView.snp.left).offset(10)
            make.top.equalTo(topContainerBackView).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                          
        }
        address.snp.makeConstraints{ (make) -> Void in
                                                         
            make.left.equalTo(topContainerBackView.snp.left).offset(10)
            make.top.equalTo(orderId.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                          
        }
        serviceTypeTitle.snp.makeConstraints{ (make) -> Void in
                                                                
            make.left.equalTo(topContainerBackView.snp.left).offset(10)
            make.top.equalTo(address.snp.bottom).offset(10)
            make.right.equalTo(topSeparator1.snp.left).offset(-10)
                                                 
        }
        serviceType.snp.makeConstraints{ (make) -> Void in
                                                                
            make.left.equalTo(topContainerBackView.snp.left).offset(10)
            make.top.equalTo(serviceTypeTitle.snp.bottom).offset(10)
            make.right.equalTo(topSeparator1.snp.left).offset(-10)
                                                 
        }
        topSeparator1.snp.makeConstraints{ (make) -> Void in
                     
            make.top.equalTo(address.snp.bottom)
            make.centerX.equalTo(topContainerView.snp.centerX)
            make.height.equalTo(65)
        }
        mobileTitle.snp.makeConstraints{ (make) -> Void in
                                                                       
            make.left.equalTo(topSeparator1.snp.right).offset(10)
            make.top.equalTo(address.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                        
        }
        mobile.snp.makeConstraints{ (make) -> Void in
                                                                       
            make.left.equalTo(topSeparator1.snp.right).offset(10)
            make.top.equalTo(mobileTitle.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                        
        }
        openDateTitle.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topContainerBackView).offset(10)
            make.top.equalTo(topSeparator1.snp.bottom).offset(10)
            make.right.equalTo(topSeparator2.snp.left).offset(-10)
                                                               
        }
        openDate.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topContainerBackView).offset(10)
            make.top.equalTo(openDateTitle.snp.bottom).offset(10)
            make.right.equalTo(topSeparator2.snp.left).offset(-10)
                                                               
        }
        allotedHoursTitle.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topSeparator2.snp.right).offset(10)
            make.top.equalTo(topSeparator1.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                               
        }
        allotedHours.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topSeparator2.snp.right).offset(10)
            make.top.equalTo(allotedHoursTitle.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                               
        }
        topSeparator2.snp.makeConstraints{ (make) -> Void in
                     
            make.top.equalTo(topSeparator1.snp.bottom).offset(10)
            make.centerX.equalTo(topContainerView.snp.centerX)
            make.height.equalTo(65)
        }
        appointmentTitle.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topContainerBackView).offset(10)
            make.top.equalTo(topSeparator2.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                               
        }
        appointment.snp.makeConstraints{ (make) -> Void in
                                                                              
            make.left.equalTo(topContainerBackView.snp.left).offset(10)
            make.top.equalTo(appointmentTitle.snp.bottom).offset(10)
            make.right.equalTo(topContainerBackView.snp.right).offset(-10)
                                                               
        }
    }

    func onGetOrderDetails(error: String) {
        
        navigationItem.title = mainViewModel!.orderDetails.client_order_no
        firstName.text = mainViewModel!.orderDetails.first_name
        lastName.text = mainViewModel!.orderDetails.last_name
        address1.text = mainViewModel!.orderDetails.address1
        address2.text = mainViewModel!.orderDetails.address2
        country.text = mainViewModel!.orderDetails.country
        state.text = mainViewModel!.orderDetails.state
        city.text = mainViewModel!.orderDetails.city
        zip.text = mainViewModel!.orderDetails.zipcode
        phone.text = mainViewModel!.orderDetails.mobile
        home.text = mainViewModel!.orderDetails.home_phone
        claimType.text = mainViewModel!.orderDetails.claim_type
        orderId.text = mainViewModel!.orderDetails.client_order_no
        address.text = mainViewModel!.orderDetails.address1 ?? "" + "," + mainViewModel!.orderDetails.zipcode
        serviceType.text = mainViewModel!.orderDetails.service_name
        mobile.text = mainViewModel!.orderDetails.mobile
        openDate.text = mainViewModel!.orderDetails.open_date
        allotedHours.text = mainViewModel!.orderDetails.allotted_hours
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MM/dd/yyyy"

        let date = dateFormatterGet.date(from: mainViewModel!.orderDetails.apntmnt_date)
        appointment.text = dateFormatterPrint.string(from: date!) + "-" + mainViewModel!.orderDetails.apntmnt_time
        
        activityIndicator.stopAnimating()
        
    }
    
    @objc func goMap() {
    
        performSegue(withIdentifier: "mapSegue", sender: "map")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        let viewController = segue.destination as? MapViewController
            
        viewController!.destinationLng = Double(self.mainViewModel!.orderDetails.lng)
            
        viewController!.destinationLat = Double(self.mainViewModel!.orderDetails.lat)
        
    }
    
}

extension CALayer {

    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat, width: CGFloat) {

        let border = CALayer()

        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.height, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }

        border.backgroundColor = color.cgColor;

        self.addSublayer(border)
    }

}
